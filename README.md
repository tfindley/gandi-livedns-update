# Gandi LiveDNS Update

Update Gandi DNS Entries via the LiveDNS API like a DDNS (Dynamic DNS) Service.

Based on original code / idea from:
https://nextnet.top/content/using-gandi-livedns-dynamic-dns-server
https://virtuallytd.com/post/dynamic-dns-using-gandi/

Gandi LiveDNS Documentation: https://api.gandi.net/docs/livedns/

# Prerequisites

You'll need your Gandi LiveDNS API Key

You can generate your API Key from the “Security” section in the Account Admin Panel

# Package Requirements

Packages required by this script:
- curl
- jq

Tested under bash on Debian and zsh on MacOS / Darwin

# External connectivity requirements

This script will commmunicate with akamai to fetch your external IP, and with Gandi to interact with the LiveDNS REST API.
- http://whatismyip.akamai.com/ (GET)
- https://api.gandi.net (GET and PUT)