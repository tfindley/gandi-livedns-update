#!/bin/bash

# Fetch current external IP address from Akamai server
MY_IP=$(curl -s http://whatismyip.akamai.com/)

if [[ $? != 0 ]]; then
	echo "There was an error quering the external IP"
	exit 1
fi

# Gandi LiveDNS API KEY
APIKEY="................"

# Static Domain hosted at Gandi
DOMAIN="................"

# Dynamic Subdomain
SUBDOMAIN="............."

# Get the current IP for the provided subdomain
SET_IP=$(curl -s -H "Authorization: Apikey $APIKEY" https://api.gandi.net/v5/livedns/domains/$DOMAIN/records/$SUBDOMAIN | jq -r '.[0] .rrset_values[0]')

if [[ $? != 0 ]]; then
	echo "There was an error quering Gandi's LiveAPI to retrieve the subdomain's current IP"
	exit 1
fi

if [[ $SET_IP != $MY_IP ]]; then

# Get the current Zone for the provided domain
	CURRENT_ZONE_HREF=$(curl -s -H "X-Api-Key: $APIKEY" https://dns.api.gandi.net/api/v5/domains/$DOMAIN | jq -r '.zone_records_href')
	if [[ $? != 0 ]]; then
		echo "There was an error quering Gandi's LiveAPI to retrieve the domain's zone record href"
		exit 1
	fi

# Update the A Record of the Subdomain by PUTing on the current zone
	curl -s -X PUT -H "Content-Type: application/json" \
	        -H "X-Api-Key: $APIKEY" \
	        -d "{\"rrset_name\": \"$SUBDOMAIN\",
	             \"rrset_type\": \"A\",
	             \"rrset_ttl\": 1200,
	             \"rrset_values\": [\"$MY_IP\"]}" \
	        $CURRENT_ZONE_HREF/$SUBDOMAIN/A | jq -r '.message'

	if [[ $? != 0 ]]; then
		echo ''
		echo "Record not successfully updated"
		exit 1
	else
		echo ''
		echo "$SUBDOMAIN.$DOMAIN"
		echo "From: $SET_IP"
		echo "  To: $MY_IP"
		exit 0
	fi

else

	echo "Record for $SUBDOMAIN.$DOMAIN is already set to the current External IP ($MY_IP)"
	exit 0

fi

echo "An expected error has occoured"
exit 1
