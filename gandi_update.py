#!/usr/bin/env python

import os
import sys
import tldextract
import requests
import json
import click

@click.command()
@click.option('-d', '--domain', envvar='DOMAIN', help='Domain to update', required=True)
@click.option('-a', '--apikey', envvar='APIKEY', help='API Key', required=True)
@click.option('-u', '--apiurl', envvar='APIURL', help='API URL', required=True, default='https://dns.api.gandi.net/api/v5/domains/')
def main(domain, apikey, apiurl):

    # if not DOMAIN:
    #     sys.exit('Domain not provided, unable to continue')

    MYIP = requests.get(url = "http://whatismyip.akamai.com").content.decode('utf-8')

    if not MYIP:
        sys.exit('Unable to get My IP')

    uri = (tldextract.extract(domain))

    if not uri.domain:
        sys.exit('uri.domain is empty - domain may be malformed')

    if not uri.suffix:
        sys.exit('quri.suffix is empty - domain may be malformed')

    if not uri.subdomain:
        sys.exit('Subdomain not provided - Currently unable to alter domains on Gandi')

    tld = uri.domain + "." + uri.suffix

    GANDI_APIKEY = apikey
    GANDI_APIURL = apiurl + tld

    if not GANDI_APIKEY:
        sys.exit('GANDI_APIKEY variable is empty')

    if not GANDI_APIURL:
        sys.exit('GANDI_APIURL variable is empty')

    HEADERS = {
        "X-Api-Key": GANDI_APIKEY,
        "Content-Type": "application/json",

    }

    if not HEADERS:
        sys.exit('HEADERS is empty')

    req = requests.get(url = GANDI_APIURL, headers = HEADERS)
    current_zone_url=(req.json()[u'domain_records_href'])

    if not current_zone_url:
        sys.exit('current_zone_url variable is empty')

    if not uri.subdomain:
        PAYLOAD = {
        'rrset_name': '*',
        'rrset_type': 'A',
        'rrset_ttl': 10800,
        'rrset_values': [
        MYIP
        ]
        }
        puturl = current_zone_url + "/A"
    else:
        PAYLOAD = {
        'rrset_name': uri.subdomain,
        'rrset_type': 'A',
        'rrset_ttl': 1200,
        'rrset_values': [
        MYIP
        ]
        }
        puturl = current_zone_url + "/" + uri.subdomain + "/A"

    if not PAYLOAD:
        sys.exit('PAYLOAD is empty')

    print( "" )
    print( "        Domain variables")
    print( "        ----------------")
    print( "                  Suffix: " + uri.suffix)
    print( "                  Domain: " + uri.domain)
    print( "               Subdomain: " + uri.subdomain)
    print( "" )
    print( "        Gandi variables")
    print( "        ---------------")
    print( "                  API URL: " + GANDI_APIURL)
    print( "        Current Zone HREF: " + current_zone_url)
    print( "            Target Record: " + puturl)
    print( "           My External IP: " + MYIP)

    put = requests.put(url = puturl, data=json.dumps(PAYLOAD), headers = HEADERS )
    print( "" )
    print(put.status_code)

if __name__ == '__main__':
    main()